<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('transisi')->group(function() {
    Route::get('/', 'TransisiController@index');
});

Route::middleware(['auth'])->group(function () {
    // Route::get('employees/company/{id}/pdf', 'EmployeeController@exportPdf')->name('employees.pdf');
    Route::get('employees/company/{id}', 'EmployeeController@filter')->name('employees.filter');
    Route::post('employees/import', 'EmployeeController@importExcel')->name('employees.import_excel');
    Route::resource('employees', EmployeeController::class);
    
    Route::get('companies/export/{id}', 'CompanyController@exportPdf')->name('companies.export.pdf');
    Route::resource('companies', 'CompanyController');
    
    Route::post('companies/get-companies', 'CompanyController@getCompanies')->name('companies.getCompanies');
});

