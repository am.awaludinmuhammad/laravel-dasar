<?php

namespace Modules\Transisi\Imports;

use Illuminate\Support\Collection;
use Modules\Transisi\Entities\Employee;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeeImport implements ToCollection
{
    public function chunkSize(): int
    {
        return 10;
    }

    public function collection(Collection $rows)
    {
        $totalRows = count($rows);
        if ($totalRows < 100 ) {
            return redirect('employees.index')->with('error', 'Import gagal. Masukkan minimal 100 data');
        } else {

            $employee = new Employee();

            foreach ($rows as $row) 
            {
                $data = [
                    'name' => $row[1],
                    'company_id' => $row[2], 
                    'status' => $row[3],
                    'email' => $row[4],
                ];
                $employee->insert($data);
            }
        }
    }

    public function rules(): array
    {
        return [
            '3' => 'unique:employees, email',
        ];
    }
}
