<?php
namespace Modules\Transisi\Constants;

class Status
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const BLOCK = 3;

    public static function labels(): array
    {
        return [
            "Aktif" => self::ACTIVE,
            "Tidak Aktif" => self::INACTIVE,
            "Di Block" => self::BLOCK,
        ];
    }

    public static function label(int $id) 
    {
        return array_search($id, self::labels());
    }
}
