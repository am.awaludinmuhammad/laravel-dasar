@php
  use Modules\Transisi\Constants\Status;
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
	</style>
  <title>Employees</title>
</head>
<body>
  <div class="container">
    <h3 style="text-align:center; margin-bottom:3rem;">Data Employee Perusahaan {{ $companyName }}</h3>
  <div class="table-responsive">
        <table border="1" style="width: 100%">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">Company</th>
              <th scope="col">Email</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach($employees as $employee)
            <tr>
              <th>{{ $loop->iteration }}</th>
              <td>{{ $employee->employee_name }}</td>
              <td>{{ $employee->company_name }}</td>
              <td>{{ $employee->email }}</td>
              <td>{{Status::label($employee->status)}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
</body>
</html>