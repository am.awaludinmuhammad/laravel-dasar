@extends('transisi::layouts.master')
@section('content')
<div class="container">

<!-- For defining autocomplete -->

  <div class="card">
    <div class="card-header">
      Tambah Employee
    </div>
    <div class="card-body">

      <form action="{{ route('employees.store') }}" method="post">@csrf
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control" name="name" value="{{ old('name') }}">
          @error('name')
          <span class="text-danger">
          {{ $errors->first('name') }}
          </span>
          @enderror
        </div>

        <!-- select2 -->
        <div class="form-group">
          <label for="exampleFormControlSelect2">Company</label>
          <select id="selectCompany" class="form-control" name="company_id">
            <option selected disabled>-- Pilih --</option>
          </select>
          @error('company_id')
          <span class="text-danger">
          {{ $errors->first('company_id') }}
          </span>
          @enderror
        </div>
        <!-- / select -->

        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" name="email" value="{{ old('email') }}">
          @error('email')
          <span class="text-danger">
          {{ $errors->first('email') }}
          </span>
          @enderror
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
@push('scripts')
 <!-- Script -->
 <script type="text/javascript">
  let CSRF_TOKEN = "{{ csrf_token() }}"; 
  $(document).ready(function(){

    $("#selectCompany").select2({
      ajax: { 
        url: "{{ route('companies.getCompanies') }}",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            _token: CSRF_TOKEN,
            search: params.term // search term
          };
        },
        processResults: function (response) {
          return {
            results: response
          };
        },
        cache: true
      }

    });

  });
</script>
@endpush
@endsection