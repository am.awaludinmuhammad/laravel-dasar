@extends('transisi::layouts.master')

@section('content')
@php
  use Modules\Transisi\Constants\Status;
@endphp

<div class="container">
@if(Session::has('success'))
  <div class="alert alert-success">
      {{ Session::get('success') }}
      @php
          Session::forget('success');
      @endphp
  </div>
@endif
  <div class="card">
    <div class="card-header">
      Data Company
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <a href="{{ asset('company/'.$company->logo) }}" target="_blank" title="Lihat Foto"><img src="{{ asset('company/'.$company->logo) }}" alt="" style="height: 64px"></a>
          <div>Name: {{$company->name}}</div>
          <div>Email: {{$company->email}}</div>
          <div>Website: {{$company->website}}</div>
        </div>
      </div>

      <hr>
      <div class="row">
        <div class="col-12">
          <h5>Employees</h5>
          <div class="d-flex justify-content-end">
            <a href="{{ route('companies.export.pdf', $company->id) }}" class="btn btn-primary btn-sm my-2 pull-right">Export PDF</a>
          </div>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Email</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>
                @foreach($employees as $employee)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$employee->name}}</td>
                  <td>{{$employee->email}}</td>
                  <td>{{Status::label($employee->status)}}</td>
                </tr>
                @endforeach
              
              </tbody>
            </table>
          </div>
          {{ $employees->links() }}
        </div>
      </div>
    
    </div>
  </div>
</div>
@endsection
