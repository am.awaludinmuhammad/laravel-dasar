@extends('transisi::layouts.master')

@section('content')
<div class="container">
@if(Session::has('success'))
  <div class="alert alert-success">
      {{ Session::get('success') }}
      @php
          Session::forget('success');
      @endphp
  </div>
@endif
  <div class="card">
    <div class="card-header">
      Data Company
    </div>
    <div class="card-body">
      <div class="d-flex justify-content-end">
        <a href="{{ route('companies.create') }}" class="btn btn-primary btn-sm my-2 pull-right">Tambah Company</a>
      </div>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">Email</th>
              <th scope="col">Logo</th>
              <th scope="col">Website</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($companies as $company)
            <tr>
              <th>{{ $loop->iteration }}</th>
              <td>{{ $company->name }}</td>
              <td>{{ $company->email }}</td>
              <td>
                <a href="{{ asset('company/'.$company->logo) }}" target="_blank" title="Lihat Foto"><img src="{{ asset('company/'.$company->logo) }}" alt="" style="height: 30px"></a>
              </td>
              <td>{{ $company->website }}</td>
              <td>
                <a href="{{ route('companies.edit', $company->id) }}" class="btn btn-sm btn-warning">Edit</a>
                <a href="{{ route('companies.show', $company->id) }}" class="btn btn-sm btn-warning">Detail</a>
                <form action="{{ route('companies.destroy', $company) }}" method="post" style="display:inline">
                @csrf @method('DELETE')
                  <button class="btn btn-danger btn-sm" type="submit">Hapus</button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $companies->links() }}
    </div>
  </div>
</div>
@endsection
