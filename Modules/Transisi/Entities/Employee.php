<?php

namespace Modules\Transisi\Entities;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name', 'company_id', 'email', 'status'];
    
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
