<?php

namespace Modules\Transisi\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Support\Renderable;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Http\Requests\CompanyStoreRequest;
use Modules\Transisi\Http\Requests\CompanyUpdateRequest;

class CompanyController extends Controller
{

    public function __construct(CompanyRepository $company)
    {
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $companies = $this->company->all();
        return response()->json([
            'success' => TRUE,
            'data' => $companies
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CompanyStoreRequest $request)
    {
        try {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $imgName = time(). '-' .\Str::slug($request->name) . '-' . 'logo.'.$extension;
            $path = Storage::putFileAs('company', $request->file('logo'), $imgName);

            $collection = [
                'name' => $request->name,
                'email' => $request->email,
                'logo' => $imgName,
                'website' => $request->website
            ];

            $this->company->store($collection);

            return response()->json([
                'success'=> TRUE, 
                'message'=> 'The company has been created successfully.', 
                'data'=> $collection
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        } 

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $company = $this->company->find($id);
            if ($company) {
                return response()->json([
                    'success'=> TRUE, 
                    'data'=> $company
                ]);
            } else {
                return response()->json([
                    'success'=> FALSE, 
                    'message'=> 'Company ID not found'
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        } 
    }


    public function update(CompanyUpdateRequest $request, $id)
    {
        try {
            if ($request->hasFile('logo')) {
                $extension = $request->file('logo')->getClientOriginalExtension();
                $imgName = time(). '-' .\Str::slug($request->name) . '-' . 'logo.'.$extension;
                $path = Storage::putFileAs('company', $request->file('logo'), $imgName);
    
                $collection = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'website' => $request->website,
                    'logo' => $imgName
                ];            
            } else {
                $collection = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'website' => $request->website
                ];
            }
    
            $this->company->update($id, $collection);
    
            return response([
                'success'=> TRUE, 
                'message'=> 'The company has been updated successfully.', 
                'data'=> $collection
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $company = $this->company->find($id);
            if ($company) {
                $this->company->destroy($id);
                return response([
                    'success'=> TRUE, 
                    'message'=> 'The company has been deleted successfully.', 
                ]);
            } else {
                return response([
                    'success'=> FALSE, 
                    'message'=> 'Company not found.', 
                ], 404);
            }    
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }
        
    }
}
