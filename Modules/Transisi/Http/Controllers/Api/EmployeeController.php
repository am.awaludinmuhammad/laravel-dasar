<?php

namespace Modules\Transisi\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Support\Renderable;
use Modules\Transisi\Repositories\EmployeeRepository;
use Modules\Transisi\Http\Requests\EmployeeStoreRequest;
use Modules\Transisi\Http\Requests\EmployeeUpdateRequest;

class EmployeeController extends Controller
{
    public function __construct(EmployeeRepository $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $employees = $this->employee->fetch($request->all());
        return response()->json([
            'success' => TRUE,
            'data' => $employees
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(EmployeeStoreRequest $request)
    {
        try {
            $collection = $request->except(['_token','_method']);
            $employee = $this->employee->store($collection);
    
            if ($employee) {
                return response()->json([
                    'success'=> TRUE, 
                    'message'=> 'The employee has been created successfully.', 
                    'data'=> $collection
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }       
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $employee = $this->employee->find($id);

            if ($employee) {
                return response()->json([
                    'success'=> TRUE, 
                    'data'=> $employee
                ]);
            } else {
                return response()->json([
                    'success'=> FALSE, 
                    'message'=> 'Employee ID not found' 
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        try {
            $collection = $request->except(['_token','_method']);
            $this->employee->update($id, $collection);
    
            return response()->json([
                'success'=> TRUE, 
                'message'=> 'The Employee has been updated successfully.', 
                'data'=> $collection
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $employee = $this->employee->find($id);
            if ($employee) {
                $this->employee->destroy($id);
                return response([
                    'success'=> TRUE, 
                    'message'=> 'The employee has been deleted successfully.', 
                ]);
            }else {
                return response([
                    'success'=> FALSE, 
                    'message'=> 'Employee not found.', 
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success'=> FALSE, 
                'message'=> $e->getMessage()
            ], 500);
        }

    }
}
