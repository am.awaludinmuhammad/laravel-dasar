<?php

namespace Modules\Transisi\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Transisi\Entities\Employee;
use Illuminate\Contracts\Support\Renderable;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Http\Requests\CompanyStoreRequest;
use Modules\Transisi\Http\Requests\CompanyUpdateRequest;

class CompanyController extends Controller
{

    public function __construct(CompanyRepository $company)
    {
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $companies = $this->company->all();
        return view('transisi::companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::companies.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CompanyStoreRequest $request)
    {
        $extension = $request->file('logo')->getClientOriginalExtension();
        $imgName = time(). '-' .\Str::slug($request->name) . '-' . 'logo.'.$extension;
        $path = Storage::putFileAs('company', $request->file('logo'), $imgName);

        $collection = [
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $imgName,
            'website' => $request->website
        ];

        $this->company->store($collection);

        return redirect()->route('companies.index')->with('success', 'Input Berhasil');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $company = $this->company->find($id);
        $employees = $this->company->employees($id);
        return view('transisi::companies.show', compact('company', 'employees'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $company = $this->company->find($id);

        return view('transisi::companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        if ($request->hasFile('logo')) {
            $extension = $request->file('logo')->getClientOriginalExtension();
            $imgName = time(). '-' .\Str::slug($request->name) . '-' . 'logo.'.$extension;
            $path = Storage::putFileAs('company', $request->file('logo'), $imgName);

            $collection = [
                'name' => $request->name,
                'email' => $request->email,
                'website' => $request->website,
                'logo' => $imgName
            ];            
        } else {
            $collection = [
                'name' => $request->name,
                'email' => $request->email,
                'website' => $request->website
            ];
        }

        $this->company->update($id, $collection);


        return redirect()->route('companies.index')->with('success', 'Update Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $company = $this->company->find($id);
        if ($company) {
            $this->company->destroy($id);
            return redirect()->route('companies.index')->with('success', 'Hapus Berhasil');
        } else {
            return redirect()->route('companies.index')->with('error', 'Error');
        }
    }

    // for ajax
    public function getCompanies(Request $request)
    {
        $keyword = $request->search;
        if($keyword == ''){
            $companies = $this->company->all();
        } else {
            $companies = $this->company->searchCompanies($keyword);
        }

        foreach($companies as $company){
            $response[] = array(
                    "id"=>$company->id,
                    "text"=>$company->name,
            );
            }
    
            return response()->json($response);
    }

    public function exportPdf($id)
    {
        $company = $this->company->find($id);

        $data['employees'] = $this->company->employeesForExport($id);       
        $data['companyName'] = $company->name;
        
        $pdf = PDF::loadView('transisi::exports.employees', $data);
        
        return $pdf->download('employee.pdf');
    }

}
