<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Transisi\Constants\Status;
use Illuminate\Contracts\Support\Renderable;
use Modules\Transisi\Imports\EmployeeImport;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Repositories\EmployeeRepository;
use Modules\Transisi\Http\Requests\ImportExcelRequest;

class EmployeeController extends Controller
{
    public function __construct(CompanyRepository $company, EmployeeRepository $employee)
    {
        $this->employee = $employee;
        $this->company = $company;
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        $companies = $this->company->all();
        $employees = $this->employee->fetch($request->all());

        return view('transisi::employees.index', compact('companies','employees'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('transisi::employees.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('transisi::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('transisi::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $this->employee->destroy($id);

        return redirect()->route('employees.index')->with('success', 'Hapus data Berhasil');
    }

    public function importExcel(ImportExcelRequest $request) 
	{
        $file = $request->file('file');
        
        $fileName = time().$file->getClientOriginalName();
		$file->move('employee',$fileName);

		$import =  Excel::import(new EmployeeImport, public_path('/employee/'.$fileName));
 
		return redirect()->route('employees.index');
	}
}
