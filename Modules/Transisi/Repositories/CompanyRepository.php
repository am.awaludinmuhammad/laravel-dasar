<?php
namespace Modules\Transisi\Repositories;

use File;
use Modules\Transisi\Entities\Company;
use Illuminate\Support\Facades\Storage;
use Modules\Transisi\Entities\Employee;

class CompanyRepository
{   
    protected $model;

    public function __construct(Company $model, Employee $employee)
    {
        $this->model = $model;
        $this->employee = $employee;
    }

    public function all()
    {
        return $this->model->query()->paginate(5);
    }

    public function fetchAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function store($collection){

        $company = new $this->model;
        $company->name = $collection['name'];
        $company->email = $collection['email'];
        $company->logo = $collection['logo'];
        $company->website = $collection['website'];

        return $company->save();
    }

    public function update($id, $collection)
    {
        $company = $this->model->find($id);
        return $company->update($collection);
    }
    
    public function destroy($id)
    {
        return $this->model->find($id)->delete();
    }

    public function employees($company_id)
    {
        return $this->employee->where('company_id', $company_id)->paginate(5);
            
    }

    public function employeesForExport($company_id)
    {
        return \DB::select("SELECT employees.id as employee_id, 
                                employees.name AS employee_name,
                                companies.name AS company_name,
                                employees.email,
                                employees.status
                            FROM employees
                            JOIN companies
                            ON employees.company_id = companies.id
                            WHERE company_id = $company_id");
            
    }

    
    // fetch companies for ajax
    public function searchCompanies($keyword, $type)
    {
        return $this->model->select('id','name')
        ->where('name', 'like', '%' .$keyword . '%')
        ->limit(5)
        ->get();

        // $q = $this->model->select('id','name')
        //     ->where('name', 'like', '%' .$keyword . '%');

        // if ($type == 'data') {
        //     return $q->get();
        // } else {
        //     return $q->count();
        // }
        

    }
    

}