<?php
namespace Modules\Transisi\Repositories;

use App\Imports\EmployeeImport;
use Modules\Transisi\Constants\Status;
use Modules\Transisi\Entities\Employee;
use Maatwebsite\Excel\Facades\Excel as Excel;

class EmployeeRepository
{   
    protected $model;

    public function __construct(Employee $model)
    {
        $this->model = $model;
    }
    
    public function fetch(array $params)
    {
        $query = $this->model->query();

        if (isset($params['status'])) {
            $query->where('status', $params['status']);
        }

        return $query = $query->paginate(5);
    }

    public function all()
    {
        return $this->model->query()->paginate(5);
    }

    public function find($id)
    {
      return $this->model->find($id);
    }

    public function store($collection)
    {
        $employee = new Employee;
        $employee->name = $collection['name'];
        $employee->company_id = $collection['company_id'];
        $employee->email = $collection['email'];
        $employee->status = $collection['status'];

        return $employee->save();
    }

    public function update($id, $collection)
    {
        $employee = $this->find($id);

        return $employee->update($collection);
    }
    
    public function destroy($id)
    {
        return $this->model->find($id)->delete();
    }


    public function importExcel($file)
    {
		$fileName = time().$file->getClientOriginalName();
		$file->move('employee',$fileName);

		$import =  Excel::import(new EmployeeImport, public_path('/employee/'.$fileName));
    }

    public function filter($id)
    {
        return Employee::where('company_id', $id)->paginate(5);
    }
}