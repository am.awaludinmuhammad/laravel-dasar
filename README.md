## Installation

- Install Composer Dependencies
    ```sh 
        composer install
     ```
- Install NPM Dependencies
    ```sh 
        npm install
     ```
- Setup env
- Generate app encryption key
    ```sh 
        php artisan key:generate
     ```
- Run Migration
    ```sh 
        php artisan migrate
     ```
     
- Seed the database
    ```sh 
        php artisan db:seed
     ```
