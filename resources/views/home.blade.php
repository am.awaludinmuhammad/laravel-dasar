@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Menu Management') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        <a href="{{ route('companies.index') }}" class="list-group-item list-group-item-action">Company</a>
                        <a href="{{ route('employees.index') }}" class="list-group-item list-group-item-action">Employee</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
