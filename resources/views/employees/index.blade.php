@extends('layouts.app')

@section('content')
<div class="container">
@if(Session::has('success'))
  <div class="alert alert-success">
      {{ Session::get('success') }}
      @php
          Session::forget('success');
      @endphp
  </div>
@endif
@if(Session::has('error'))
  <div class="alert alert-danger">
      {{ Session::get('error') }}
      @php
          Session::forget('error');
      @endphp
  </div>
@endif
  <div class="card">
    <div class="card-header">
      Data Employee
    </div>
    <div class="card-body">
    <div class="dropdown">
      <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Filter Company
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      @foreach($companies as $company)
        <a class="dropdown-item" href="{{ route('employees.filter', $company->id) }}">{{ $company->name }}</a>
      @endforeach
      {{$companies->links()}}
      </div>
    </div>
    
      <div class="d-flex justify-content-end">
        <a href="{{ route('employees.create') }}" class="btn btn-primary btn-sm my-2 pull-right">Tambah Employee</a>
        <div class="dropdown">
          <button class="btn btn-secondary btn-sm dropdown-toggle my-2 ml-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Export PDF
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          @foreach($companies as $company)
            <a class="dropdown-item" href="{{ route('employees.pdf', $company->id) }}">{{ $company->name }}</a>
          @endforeach
          {{$companies->links()}}
          </div>
        </div>

        <button type="button" class="btn btn-sm btn-success my-2 ml-2" data-bs-toggle="modal" data-bs-target="#importExcel">
          Import Excel
        </button>
      </div>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">Company</th>
              <th scope="col">Email</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($employees as $employee)
            <tr>
              <th>{{ $loop->iteration }}</th>
              <td>{{ $employee->name }}</td>
              <td>{{ $employee->company->name }}</td>
              <td>{{ $employee->email }}</td>
              <td>
                <a href="{{ route('employees.edit', $employee) }}" class="btn btn-sm btn-warning">Edit</a>
                <form action="{{ route('employees.destroy', $employee) }}" method="post" style="display:inline">
                @csrf @method('DELETE')
                  <button class="btn btn-danger btn-sm" type="submit">Hapus</button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $employees->links() }}
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="{{ route('employees.import_excel') }}" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
 
							{{ csrf_field() }}
 
							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file">
                @error('file')
                <span class="text-danger">
                {{ $errors->first('file') }}
                </span>
                @enderror
							</div>
 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>
@endsection

