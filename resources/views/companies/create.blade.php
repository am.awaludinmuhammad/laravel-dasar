@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      Tambah Company
    </div>
    <div class="card-body">

      <form action="{{ route('companies.store') }}" method="post" enctype="multipart/form-data">@csrf
        <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control" name="name" value="{{ old('name') }}">
          @error('name')
          <span class="text-danger">
          {{ $errors->first('name') }}
          </span>
          @enderror
        </div>

        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" name="email" value="{{ old('email') }}">
          @error('email')
          <span class="text-danger">
          {{ $errors->first('email') }}
          </span>
          @enderror
        </div>

        <div class="form-group">
          <label for="logo">Logo</label>
          <input type="file" class="form-control-file" name="logo">
          <span class="text-danger d-block">*minimum 100x100 px, png, ukuran maks 2 MB</span>
          @error('logo')
          <span class="text-danger">
          {{ $errors->first('logo') }}
          </span>
          @enderror
        </div>

        <div class="form-group">
          <label for="name">Website</label>
          <input type="text" class="form-control" name="website" value="{{ old('website') }}">
          @error('website')
          <span class="text-danger">
          {{ $errors->first('website') }}
          </span>
          @enderror
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
@endsection