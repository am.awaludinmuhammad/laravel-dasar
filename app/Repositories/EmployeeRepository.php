<?php
namespace App\Repositories;

use App\Employee;
use App\Imports\EmployeeImport;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Repositories\EmployeeRepositoryInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{   
    protected $employee = null;

    public function all()
    {
        return Employee::paginate(5);
    }

    public function find($id)
    {
        return Employee::find($id);
    }

    public function store($collection)
    {
        $employee = new Employee;
        $employee->name = $collection['name'];
        $employee->company_id = $collection['company_id'];
        $employee->email = $collection['email'];

        return $employee->save();
    }

    public function update($id, $collection)
    {
        $employee = $this->find($id);

        return $employee->update($collection);
    }
    
    public function destroy($id)
    {
        return Employee::find($id)->delete();
    }

    public function importExcel($file)
    {
		$fileName = time().$file->getClientOriginalName();
		$file->move('employee',$fileName);

		$import =  Excel::import(new EmployeeImport, public_path('/employee/'.$fileName));
        // dd('Row count: ' . $import->getRowCount());
    }

    public function filter($id)
    {
        return Employee::where('company_id', $id)->paginate(5);
    }
}