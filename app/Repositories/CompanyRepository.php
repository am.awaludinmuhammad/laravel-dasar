<?php
namespace App\Repositories;

use File;
use App\Company;
use Illuminate\Support\Facades\Storage;
use App\Repositories\CompanyRepositoryInterface;

class CompanyRepository implements CompanyRepositoryInterface
{   
    protected $company = null;

    public function all()
    {
        return Company::paginate(5);
    }

    public function find($id)
    {
        return Company::find($id);
    }

    public function store($collection)
    {
        $extension = $collection['logo']->extension();
        $imgName = time(). '-' .\Str::slug($collection['name']) . '-' . 'logo' . '.'.$extension;
        $path = Storage::putFileAs('company', $collection['logo'], $imgName);

        $company = new Company;
        $company->name = $collection['name'];
        $company->email = $collection['email'];
        $company->logo = $imgName;
        $company->website = $collection['website'];

        return $company->save();
    }

    public function update($id, $collection)
    {
        $company = Company::find($id);
        
        if (array_key_exists('logo', $collection)) {
            File::delete('company/'.$company->logo);
            
            $extension = $collection['logo']->extension();
            $imgName = time(). '-' .\Str::slug($collection['name']) . '-' . 'logo' . '.'.$extension;
            $path = Storage::putFileAs('company', $collection['logo'], $imgName);
            
            $company->name = $collection['name'];
            $company->email = $collection['email'];
            $company->logo = $imgName;
            $company->website = $collection['website'];
            return $company->update();
        } else {
            return $company->update($collection);
        }

    }
    
    public function destroy($id)
    {
        return Company::find($id)->delete();
    }

    public function searchCompanies($keyword)
    {
        return Company::orderby('name','asc')->select('id','name')->where('name', 'like', '%' .$keyword . '%')->paginate(5);
    }
}