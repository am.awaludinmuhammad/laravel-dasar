<?php
namespace App\Repositories;

use App\Company;

interface CompanyRepositoryInterface
{
   public function all();
   public function find($id);
   public function store($collection);
   public function update($id, $collection);
   public function destroy($id);
   public function searchCompanies($keyword);
}