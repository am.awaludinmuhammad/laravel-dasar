<?php

namespace App\Imports;

use App\Employee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeeImport implements ToCollection
{
    public function chunkSize(): int
    {
        return 10;
    }

    public function collection(Collection $rows)
    {
        $totalRows = count($rows);
        if ($totalRows < 100 ) {
            return redirect('employees.index')->with('error', 'Import gagal. Masukkan minimal 100 data');
        } else {

            foreach ($rows as $row) 
            {
                Employee::create([
                    'name' => $row[1],
                    'company_id' => $row[2], 
                    'email' => $row[3],
                ]);
            }
        }
    }

    public function rules(): array
    {
        return [
            '3' => 'unique:employees, email',
        ];
    }
}
