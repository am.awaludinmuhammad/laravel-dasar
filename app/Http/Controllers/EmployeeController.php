<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Imports\EmployeeImport;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ImportExcelRequest;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Repositories\CompanyRepositoryInterface;
use App\Repositories\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    private $employee;
    private $company;

   
    public function __construct(EmployeeRepositoryInterface $employee, CompanyRepositoryInterface $company)
    {
        $this->employee = $employee;
        $this->company = $company;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->employee->all();
        $companies = $this->company->all();

        return view('employees.index', compact('employees', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = \App\Company::paginate(5);
        return view('employees.create', $companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {
        $collection = $request->except(['_token','_method']);
        $this->employee->store($collection);

        return redirect()->route('employees.index')->with('success', 'Input berhasil');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $employee = $this->employee->find($employee->id);

        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, Employee $employee)
    {
        $collection = $request->except(['_token','_method']);
        $this->employee->update($employee->id, $collection);

        return redirect()->route('employees.index')->with('success', 'Update Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $this->employee->destroy($employee->id);

        return redirect()->route('employees.index')->with('success', 'Hapus data Berhasil');
    }

    public function exportPdf($id)
    {
        $data['employees'] = $this->employee->filter($id);
  
        $company = $this->company->find($id);
        $data['companyName'] = $company->name;

        $pdf = PDF::loadView('employees.pdf', $data);
        
        return $pdf->download('employee.pdf');
    }

    public function importExcel(ImportExcelRequest $request) 
	{
		$file = $request->file('file');
        $this->employee->importExcel($file);
 
        // dd('Row count: ' . $import->getRowCount());
		return redirect()->route('employees.index');
	}

    public function filter(Request $request)
    {
        $employees = $this->employee->filter($request->id);
        $companies = $this->company->all();

        return view('employees.index', compact('employees', 'companies'));
    }


}
