<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getProvince($id_provinsi)
    {
        $response = Http::get('http://dev.farizdotid.com/api/daerahindonesia/provinsi/'.$id_provinsi);

        return $response;
    }


    public function rumusHitungLuas($p, $l)
    {
        $luas = $p * $l;

        return $luas;
    }

    public function hitungLuas()
    {
       $province =  $this->getProvince(36, 'blabal');

        return $province;
    }


}
