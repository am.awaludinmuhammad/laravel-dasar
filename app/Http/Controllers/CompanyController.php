<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Repositories\CompanyRepositoryInterface;

class CompanyController extends Controller
{
    private $company;
   
    public function __construct(CompanyRepositoryInterface $company)
    {
        $this->company = $company;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->company->all();
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        $collection = $request->except(['_token','_method']);

        $this->company->store($collection);

        return redirect()->route('companies.index')->with('success', 'Input Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        // dd($company);
        $company = $this->company->find($company->id);

        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $collection = $request->except(['_token','_method']);
        $this->company->update($company->id, $collection);

        return redirect()->route('companies.index')->with('success', 'Update Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $this->company->destroy($company->id);

        return redirect()->route('companies.index')->with('success', 'Hapus data Berhasil');
    }

    // Ajax
    public function getCompanies(Request $request)
    {
        $keyword = $request->search;
        if($keyword == ''){
            $companies = $this->company->all();
        } else {
            $companies = $this->company->searchCompanies($keyword);
        }

        foreach($companies as $company){
            $response[] = array(
                 "id"=>$company->id,
                 "text"=>$company->name,
            );
         }
   
         return response()->json($response);
    }
}
